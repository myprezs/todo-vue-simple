//app used only for reactive demo
var app;
(function (window) {
	'use strict';

	let axiosApi = axios.create({
		baseURL:'http://localhost:4000/api'
	})

	let filters = {
		all: (todos) => todos,
		completed: (todos) => todos.filter(x => x.completed),
		active: (todos) => todos.filter(x => !x.completed)
	}

	//initialize the vue app
	app = new Vue({
		el: ".todoapp",
		data: {
			title: 'vTodos',
			currentFilter: 'all',
			todos: [
				{ id: 1, title: "create todo imput", completed: true },
				{ id: 1, title: "display todo imput", completed: true },
				{ id: 1, title: "complete todo imput", completed: true },
				{ id: 1, title: "filter todo ", completed: false },
				{ id: 1, title: "backend integration todo ", completed: false }
			]
		},
		computed: {
			filteredTodos: function () {
				console.log('current filter :', this.currentFilter);			
				 return filters[this.currentFilter](this.todos); }
		},
		methods: {
			addTodo: function (ev) {
				if (ev.target.value) {
					//console.log(`creating a new todo ${ev.target.value}`);
					//this.todos.push({ id: this.todos.length + 1, title: ev.target.value, completed: false });
					axiosApi.post("/todos",{title: ev.target.value, completed: false })
					.then((resp) =>{ 
						this.todos.push(resp.data);
						ev.target.value = '';})
					.catch((err) => console.log(err));
					
				}
			},
			toggleTodo(todo) {
				axiosApi.patch(`/todos/${todo.id}/complete`)
				.then((resp)=>{
					var idx = this.todos.indexOf(todo);
					this.todos[idx].completed = !todo.completed;
				})
				.catch((err) => console.log(err));
				
			},
			filter: function (filter) { this.currentFilter = filter; },
			deleteTodo: function(todo){
				if (confirm(`delete ${todo.title}`)){
					axiosApi.delete(`/todos/${todo.id}`)
					.then((resp)=>{
						let idx = this.todos.indexOf(todo);
						this.todos.splice(idx,1);
					})
					.catch((err) => console.log(err));
					
				}
			}
		},
		created: function(){
			console.log('gettign todos');
			axiosApi.get("/todos")
			.then((resp) => {
				console.log(resp.data,this.todos );
				 this.todos = resp.data})
			.catch((err) => console.log(err));
		}
	});


})(window);
